import sys, datetime

from PyQt5.QtWidgets import (QApplication, QLabel, QMainWindow, QPushButton, QDialog, QLineEdit,
                             QWidget, QGridLayout, QCalendarWidget, QHBoxLayout, QTableWidgetItem,
                             QComboBox, QItemDelegate, QCheckBox)
from PyQt5.QtCore import QDate, pyqtSignal, pyqtSlot, QObject
from PyQt5.uic import loadUi

from ui_newRecipe import Ui_Form
from ui_addRecipe import Add_Recipe


class Communicate(QObject):

    closeApp = pyqtSignal()


class App(QMainWindow):


    def __init__(self):
        QMainWindow.__init__(self)

        loadUi("qitchen3.ui", self)
        probyem = MyClass().myprin()

        self.actionSettings.triggered.connect(self.settings)
        self.actionHelp.triggered.connect(self.help)
        self.actionExit.triggered.connect(self.myclose)
        self.actionCalendar_menu.triggered.connect(self.calendar_menu)
        self.actionOffer_recipe.triggered.connect(self.offer_recipe)
        self.actionCreate_menu.triggered.connect(self.create_menu)
        self.actionNew_recipe.triggered.connect(self.create_new_recipe)

        today = datetime.date.today()
        currentdata = (today.strftime('Показать меню на:'))

        self.list_combobox = ['штук', 'литр', 'грамм', 'килограмм', 'щепотка', 'пол чайной ложки',
                              'чайная ложка', 'половина столовой ложки', 'столовая ложка', 'стакан']

        prob = self.MyCenWin
        self.current_menu = QLabel(probyem)
        self.lbl = QLabel(self)
        zagolovok = QLabel(currentdata)
        cal = QCalendarWidget()
        date = cal.selectedDate()
        self.lbl.setText("Меню на cегодня:")
        cal.clicked[QDate].connect(self.showDate)
        self.btn_create_menu = QPushButton("Создать меню")
        self.btn_offer_recipe = QPushButton("Предложить рецепт")
        grid = QGridLayout()
        grid.addWidget(self.lbl, 0, 0)
        grid.addWidget(self.current_menu, 0, 1)
        grid.addWidget(zagolovok, 1, 0)
        grid.addWidget(cal, 1, 1)
        grid.addWidget(self.btn_create_menu)
        grid.addWidget(self.btn_offer_recipe)
        prob.setLayout(grid)
        self.setCentralWidget(prob)

        self.statusbar.showMessage((today.strftime('Сегодняшний день %d, %b %Y')))

    def showDate(self, date):
        # print(date)
        insday = date.day()
        ready_menu = {1: "нету", 2: "пюре, компот", 3: "борщ", 4: "сосиски", 5: "рагу", 6: "суп",
                      7: "нету", 22: "This is today", 25: "варенье"}
        for i in ready_menu:
            if i == insday:
                otvet = ready_menu.get(i)
                break
            else:
                otvet ="yps"

        ppp = date.toPyDate().strftime('Меню на: %d, %b %Y')
        self.lbl.setText(ppp)
        self.current_menu.setText(date.toString(otvet))

    def create_new_recipe(self, x=None):
        self.form = Ui_Form()
        create_new_recipe = QWidget()
        self.form.setupUi(create_new_recipe)
        self.plus = self.form.pushButton
        self.plus.clicked.connect(self.addprod)
        self.btnsave = self.form.pushButton_2
        self.name_recipe = self.form.lineEdit

        self.x = str(x)
        if self.x == "False":
            self.add_products = self.form.label_2
            self.add_products.setText("")
        else:
            self.add_products = self.form.label_2
            self.add_products.setText(x)

        self.method_recipe = self.form.plainTextEdit
        self.setCentralWidget(create_new_recipe)
        self.btnsave.clicked.connect(self.rec_save)


    def addprod(self, products = None):

        self.myproduct = ['картофель', 'рис', 'капуста', 'сметана', 'курица']
        vasa = self.myproduct[0]
        self.colstr = len(self.myproduct)

        self.form = Add_Recipe()
        add_products = QWidget()
        self.form.setupUi(add_products)
        self.tabl = self.form.tableWidget
        self.tabl.setRowCount(self.colstr)

        i = 0
        while i < self.colstr:
            for d in self.myproduct:
                self.item = QTableWidgetItem(d)
                self.tabl.setItem(i, 0, self.item)
                i += 1
        for i in range(self.colstr):
            self.item = QCheckBox("выбрать")
            self.tabl.setCellWidget(i, 3, self.item)
        for row in range(self.colstr):
            c = QComboBox()
            c.addItems(self.list_combobox)
            self.tabl.setCellWidget(row, 2, c)
        self.show()
        self.setCentralWidget(add_products)
        self.form.pushButton_2.clicked.connect(self.addrow)
        # self.form.pushButton.clicked.connect(lambda: self.create_new_recipe(self.vasa))
        self.form.pushButton.clicked.connect(lambda: self.create_new_recipe(self.proba()))


    def addrow(self):

        print("Создаю окно для создания нового продукта")
        self.modalWindow = QDialog()
        self.modalWindow.resize(400, 100)
        self.layr = QHBoxLayout()
        self.name = QLineEdit("Название нового продукта", self.modalWindow)
        self.btn = QPushButton("Добавить", self.modalWindow)
        self.layr.addWidget(self.name)
        self.layr.addWidget(self.btn)
        self.modalWindow.setLayout(self.layr)
        self.modalWindow.setWindowModality(0)
        self.modalWindow.show()
        self.bb = self.btn.clicked.connect(lambda: self.create_row(self.name.text()))

    def create_row(self, name_prod):

        self.c = QComboBox()
        self.c.addItems(self.list_combobox)
        self.checkBox = QCheckBox("выбрать")
        self.rowPosition = self.form.tableWidget.rowCount()
        self.form.tableWidget.insertRow(self.rowPosition)
        self.form.tableWidget.setItem(self.rowPosition, 0, QTableWidgetItem(name_prod))
        self.form.tableWidget.setCellWidget(self.rowPosition, 2, self.c)
        self.form.tableWidget.setCellWidget(self.rowPosition, 3, self.checkBox)

    def proba(self):
        self.fullrecipe = []
        dd = self.tabl
        rowcol = self.tabl.rowCount()
        for i in range(rowcol):
            vasa = dd.cellWidget(i, 3).checkState()
            if vasa == 2:
                first0 = dd.item(i, 0).text()
                first1 = dd.item(i, 1).text()
                first2 = dd.cellWidget(i, 2).currentText()
                self.fullrecipe.append([first0, first1, first2])
            products = self.fullrecipe
            # products
        return str(products)

    def proverka(self, x):
        x = self.proba()
        print(x)
        # self.addprod(products=products)
        # self..pushButton_3.name()
        # self.form.pushButton_2.clicked.connect(self.create_new_recipe)
        # return self.fullrecipe
        # self.create_new_recipe(products=self.fullrecipe)


    @pyqtSlot()
    def rec_save(self):
        namerec = self.name_recipe.text()
        metrec = self.method_recipe.toPlainText()
        print(namerec, metrec)

    @pyqtSlot()
    def settings(self):
        """настройка программы"""
        setvid = QLabel()
        setvid.setText("в разработке")
        self.setCentralWidget(setvid)

    def help(self):

        """Описание пунктов программы"""
        self.helpvid = QLabel("Help в разработке")
        self.setCentralWidget(self.helpvid)

    def myclose(self):
        """Закрытие программы"""
        myclosevid = QLabel("Exit в разработке")
        self.setCentralWidget(myclosevid)

    def calendar_menu(self):
        """Показывается при запуске программы"""
        calenvid = QLabel("Calendar menu в разработке")
        self.setCentralWidget(calenvid)

    def offer_recipe(self):
        """Предложить рецепт"""
        offer_recvid = QLabel("Offer recipe в разработке")
        self.setCentralWidget(offer_recvid)

    def create_menu(self):
        """Создать меню"""
        create_menuvid = QLabel("Create menu в разработке")
        self.setCentralWidget(create_menuvid)


class MyClass():

    today = datetime.date.today().day

    def myprin(self):
        ready_menu = {1: "нету", 2: "пюре, компот", 3: "борщ", 4: "сосиски", 5: "рагу", 6: "суп",
                      7: "нету", 22: "This is today", 25: "варенье"}
        for i in ready_menu:
            if i == self.today:
                otvet = ready_menu.get(i)
                break
            else:
                otvet ="yps"
        return otvet


class Create_Recipe():
    pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    ex.show()
    sys.exit(app.exec_())
