# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_recipe.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Add_Recipetwo(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(634, 500)
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(140, 0, 311, 31))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(370, 420, 191, 27))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(Form)
        self.pushButton_2.setGeometry(QtCore.QRect(50, 420, 181, 27))
        self.pushButton_2.setObjectName("pushButton_2")
        self.tableView = QtWidgets.QTableView(Form)
        self.tableView.setGeometry(QtCore.QRect(5, 41, 621, 371))
        self.tableView.setObjectName("tableView")
        # self.tableView.horizontalHeader('sddfsdf')

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label.setText(_translate("Form", "                 Добавить ингредиенты"))
        self.pushButton.setText(_translate("Form", "Добавить в рецепт"))
        self.pushButton_2.setText(_translate("Form", "Новый продукт"))

